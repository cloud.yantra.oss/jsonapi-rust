# jsonapi-rs

[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/cloud.yantra.oss/jsonapi-rust/-/blob/main/LICENSE-MIT.md)
[![Documentation](https://docs.rs/jsonapi-rs/badge.svg)](https://docs.rs/jsonapi-rs/latest/jsonapi_rs/)
[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgitlab.com%2Fcloud.yantra.oss%2Fjsonapi-rust.svg?type=small)](https://app.fossa.com/projects/git%2Bgitlab.com%2Fcloud.yantra.oss%2Fjsonapi-rust?ref=badge_small)

This is implementation of the JSON-API v1 specification at [jsonapi.org](http://jsonapi.org/).

* [API Documentation at docs.rs](https://docs.rs/jsonapi-rs)
* [CHANGELOG](https://gitlab.com/cloud.yantra.oss/jsonapi-rust/-/blob/main/CHANGELOG.md)

## Use

Add this crate to your _Cargo.toml_ file,

    [dependencies]
    jsonapi-rs = "*"

Or use the master branch directly from Gitlab,

    [dependencies]
    jsonapi-rs = { git = "https://gitlab.com/cloud.yantra.oss/jsonapi-rust.git", branch = "main" }

Examples of most serialization and deserialization cases can be found in the [
_tests/_](https://gitlab.com/cloud.yantra.oss/jsonapi-rust/tree/master/tests) directory or
the [documentation](https://docs.rs/jsonapi).

## Development

_Note - Until this crate reaches v1.0.0 breaking changes that are not backwards compatible will be announced in
the [CHANGELOG](https://gitlab.com/cloud.yantra.oss/jsonapi-rust/blob/master/CHANGELOG.md)._

### Testing

The command `cargo test` will run all tests. For more verbose output or output with _cargo watch_,

    RUST_BACKTRACE=1 cargo test -- --nocapture
    RUST_BACKTRACE=1 cargo watch "test -- --nocapture"

## Contributing

Contributions are welcome. Please add tests and write commit messages using
using [conventional](https://github.com/conventional-changelog/conventional-changelog/blob/a5505865ff3dd710cf757f50530e73ef0ca641da/conventions/angular.md)
format. The Changelog is updated using the [clog](https://github.com/clog-tool/clog-cli) tool. The configuration is
found in `.clog.toml`.

The current configuration works for commit messages prefixed with `feat:`, `bug:`, `test:`, `doc:` and `refactor:`.

## License
[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bgitlab.com%2Fcloud.yantra.oss%2Fjsonapi-rust.svg?type=large&issueType=license)](https://app.fossa.com/projects/git%2Bgitlab.com%2Fcloud.yantra.oss%2Fjsonapi-rust?ref=badge_large&issueType=license)
